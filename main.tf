provider "aws" {
  region = "us-east-1"
}

variable "vpc_cidr_block" {

}
variable "subnet_cidr_block" {

}
variable "avail_zone" {

}

variable "env_prefix" {

}

variable "my_ip" {}
variable "instance_type" {}
variable "public_key_location" {}

resource "aws_vpc" "archa_vpc" {
  cidr_block = var.vpc_cidr_block

  tags = {
    Name = "${var.env_prefix}-vpc"
  }
}

resource "aws_subnet" "archa_subnet_1" {
  vpc_id            = aws_vpc.archa_vpc.id
  cidr_block        = var.subnet_cidr_block
  availability_zone = var.avail_zone

  tags = {
    Name = "${var.env_prefix}-subnet-1"
  }
}


resource "aws_internet_gateway" "archa_gw" {
  vpc_id = aws_vpc.archa_vpc.id

  tags = {
    Name = "${var.env_prefix}_archa_internet_gateway"
  }
}
/*
resource "aws_route_table" "archa_rt" {
  vpc_id = aws_vpc.archa_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.archa_gw.id
  }

  tags = {
    Name = "${var.env_prefix}_archa_route_table"
  }
}
*/
/*
resource "aws_route_table_association" "archa_association" {
  subnet_id      = aws_subnet.archa_subnet_1.id
  route_table_id = aws_route_table.archa_rt.id
}
*/

resource "aws_default_route_table" "archa_igw" {
  default_route_table_id = aws_vpc.archa_vpc.default_route_table_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.archa_gw.id
  }
  tags = {
    Name = "${var.env_prefix}-rtb"
  }
}
/*
resource "aws_security_group" "archa_sg" {
  name        = "archa_sg"
  description = "Allow HTTP inbound traffic"
  vpc_id      = aws_vpc.archa_vpc.id

  ingress {
    description = "HTTP from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks  = [var.my_ip]
  }

  ingress {
    from_port  = 8080
    to_port    = 8080
    protocol   = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name = "allow_http_ssh"
  }
}
*/
resource "aws_default_security_group" "default_sg" {
  vpc_id = aws_vpc.archa_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }
  tags = {
    Name = "allow_http_ssh"
  }
}

data "aws_ami" "latest_amazon_linux_image" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_key_pair" "archa_key" {
  key_name   = "archa_key"
  public_key = file(var.public_key_location)
}

resource "aws_instance" "archa_server" {
  ami                         = data.aws_ami.latest_amazon_linux_image.id
  instance_type               = var.instance_type
  subnet_id                   = aws_subnet.archa_subnet_1.id
  vpc_security_group_ids      = [aws_default_security_group.default_sg.id]
  availability_zone           = var.avail_zone
  associate_public_ip_address = true
  key_name                    = aws_key_pair.archa_key.key_name

  /*
  user_data = <<EOF
                #!/bin/bash
                sudo yum update -y && sudo yum install -y docker
                sudo systemctl start docker
                sudo usermod -aG docker ec-user
                docker run -p 8080:80 nginx
            EOF
  */
  
  user_data = file("/home/dastan/Workspace/archa-tf-learn/entry-script.sh")
  tags = {
    Name = "archa_server"
  }
}

output "aws_ami" {
  value = data.aws_ami.latest_amazon_linux_image.id
}
output "ec2_public_ip" {
  value = aws_instance.archa_server.public_ip
}

